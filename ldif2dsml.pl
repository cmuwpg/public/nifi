#!/usr/bin/perl

use Net::LDAP::DSML;
use Net::LDAP::LDIF;

my $ldif = Net::LDAP::LDIF->new();
my $dsml = Net::LDAP::DSML->new();

$dsml->start_dsml();

while (not $ldif->eof()) {
  my $entry = $ldif->read_entry();
  die($ldif->error_lines()) if $ldif->error();
  $dsml->write_entry($entry);
}
 
$dsml->end_dsml();
