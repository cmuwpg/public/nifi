FROM apache/nifi:1.25.0

USER root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && \
    apt-get upgrade -qq -y && \
    apt-get -qq -y --no-install-recommends install weasyprint ldap-utils libnet-ldap-perl libxml-sax-writer-perl && \
    apt clean

RUN mkdir /lib/jdbc
WORKDIR /lib/jdbc

RUN curl -L -o postgresql.jar https://jdbc.postgresql.org/download/postgresql-42.7.3.jar

RUN curl -L -o mssql.tgz 'https://go.microsoft.com/fwlink/?linkid=2262683' \
    && tar -xzf mssql.tgz --strip-components=3 sqljdbc_12.6/enu/jars/mssql-jdbc-12.6.1.jre11.jar \
    && mv mssql-jdbc-12.6.1.jre11.jar mssql-jdbc.jar \
    && rm -r mssql.tgz

COPY ldif2dsml.pl /ldif2dsml.pl
COPY fonts /fonts

USER nifi
WORKDIR ${NIFI_HOME}

RUN cat conf/authorizers.xml && \
    cat conf/bootstrap.conf && \
    cat conf/logback.xml && \
    cat conf/nifi.properties && \
    cat conf/state-management.xml && \
    cat /etc/apt/sources.list
